INSERT into user(id,name,password) values (sq_user.nextval, 'samantha', '123456');
INSERT into user(id,name,password) values (sq_user.nextval, 'steve', '123456');

INSERT into account(id,number,iban,balance) values (sq_account.nextval, '123456', 'FR6598', 0);

INSERT into account_user_list(account_list_id, user_list_id) values (1,1);
INSERT into account_user_list(account_list_id, user_list_id) values (1,2);

INSERT into payment(id,name) values (sq_payment.nextval, 'carte bancaire');
INSERT into payment(id,name) values (sq_payment.nextval, 'cheque');
INSERT into payment(id,name) values (sq_payment.nextval, 'virement');
INSERT into payment(id,name) values (sq_payment.nextval, 'espece');
INSERT into payment(id,name) values (sq_payment.nextval, 'paypal');
INSERT into payment(id,name) values (sq_payment.nextval, 'prelevement');

INSERT into categorie(id,name) values (sq_categorie.nextval, 'impots et taxes');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'alimentation');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'energie');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'assurance');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'essence');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'sante');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'virement');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'transport');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'autres');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'restaurant');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'shopping');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'telephonie');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'loisirs');
INSERT into categorie(id,name) values (sq_categorie.nextval, 'epargne');

INSERT into operation(id, name, amount, negative, date_Operation, account_id, categorie_id, payment_id, user_id) values (sq_operation.nextval, 'Carrefour', 10, 0,to_timestamp('2016-12-12', 'YYYY-MM-DD'), 1, 2, 1, 1);




