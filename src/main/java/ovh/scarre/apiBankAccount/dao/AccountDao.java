package ovh.scarre.apiBankAccount.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ovh.scarre.apiBankAccount.model.Account;

public interface AccountDao extends JpaRepository<Account, Integer> {

  Account findById(int pId);

  Account findByNumber(String pNumber);

  Account findByIban(String pIban);
}
