package ovh.scarre.apiBankAccount.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ovh.scarre.apiBankAccount.model.User;

public interface UserDao extends JpaRepository<User, Integer> {

  User findById(int pId);
}
