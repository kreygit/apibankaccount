package ovh.scarre.apiBankAccount.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ovh.scarre.apiBankAccount.model.Payment;

public interface PaymentDao extends JpaRepository<Payment, Integer> {

  Payment findById(int pId);

}
