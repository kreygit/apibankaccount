package ovh.scarre.apiBankAccount.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ovh.scarre.apiBankAccount.model.Categorie;

public interface CategorieDao extends JpaRepository<Categorie, Integer> {

  Categorie findById(int pId);
}
