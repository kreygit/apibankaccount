package ovh.scarre.apiBankAccount.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import ovh.scarre.apiBankAccount.model.Operation;

public interface OperationDao extends JpaRepository<Operation, Integer>{

	Operation findById(int pId);
}
