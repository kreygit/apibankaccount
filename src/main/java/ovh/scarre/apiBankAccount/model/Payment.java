package ovh.scarre.apiBankAccount.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

@Entity
public class Payment {

  @Id
  @SequenceGenerator(name = "SQ_PAYMENT", sequenceName = "SQ_PAYMENT", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_PAYMENT")
  private Integer id;

  @Column(unique = true)
  private String name;

  public Payment() {
    super();
  }

  public Payment(Integer pId, String pName) {
    super();
    this.id = pId;
    this.name = pName;
  }

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer pId) {
    this.id = pId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String pName) {
    this.name = pName;
  }

}
