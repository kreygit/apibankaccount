package ovh.scarre.apiBankAccount.model;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Account {

  @Id
  @SequenceGenerator(name = "SQ_ACCOUNT", sequenceName = "SQ_ACCOUNT", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_ACCOUNT")
  private Integer id;

  @Basic
  private String number;

  @Basic
  private String iban;

  @Column(scale = 2)
  private Float balance = 0.0f;

  @Basic
  @ManyToMany
  private List<User> userList;

  public Account() {
    super();
  }

  public Account(Integer pId, String pNumber, String pIban, Float pBalance, List<User> pUserList) {
    super();
    this.id = pId;
    this.number = pNumber;
    this.iban = pIban;
    this.balance = pBalance;
    this.userList = pUserList;
  }

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer pId) {
    this.id = pId;
  }

  public String getNumber() {
    return this.number;
  }

  public void setNumber(String pNumber) {
    this.number = pNumber;
  }

  public String getIban() {
    return this.iban;
  }

  public void setIban(String pIban) {
    this.iban = pIban;
  }

  public Float getBalance() {
    return this.balance;
  }

  public void setBalance(Float pBalance) {
    this.balance = pBalance;
  }

  public List<User> getUserList() {
    return this.userList;
  }

  public void setUserList(List<User> pUserList) {
    this.userList = pUserList;
  }

}
