package ovh.scarre.apiBankAccount.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Operation {

	@Id
	@SequenceGenerator(name = "SQ_OPERATION", sequenceName = "SQ_OPERATION", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_OPERATION")
	private Integer id;
	
	@Basic
	private String name;
	
	@Column(scale = 2)
	private Float amount = 0f;
	
	@Basic
	private Boolean tally = false;
	
	@Basic
	private Boolean negative;
	
	@Basic
	private Date dateOperation;
	
	@Basic
	private Date dateTally = null;
	
	@ManyToOne
	private Account account;
	
	@ManyToOne
	private Categorie categorie;
	
	@ManyToOne
	private Payment payment;
	
	@ManyToOne
	private User user;

	public Operation() {
		super();
	}

	public Operation(Integer id, String name, Float amount, Boolean tally, Boolean negative, Date dateOperation,
			Date dateTally, Account account, Categorie categorie, Payment payment, User user) {
		super();
		this.id = id;
		this.name = name;
		this.amount = amount;
		this.tally = tally;
		this.negative = negative;
		this.dateOperation = dateOperation;
		this.dateTally = dateTally;
		this.account = account;
		this.categorie = categorie;
		this.payment = payment;
		this.user = user;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public Boolean getTally() {
		return tally;
	}

	public void setTally(Boolean tally) {
		this.tally = tally;
	}

	public Boolean getNegative() {
		return negative;
	}

	public void setNegative(Boolean negative) {
		this.negative = negative;
	}

	public Date getDateOperation() {
		return dateOperation;
	}

	public void setDateOperation(Date dateOperation) {
		this.dateOperation = dateOperation;
	}

	public Date getDateTally() {
		return dateTally;
	}

	public void setDateTally(Date dateTally) {
		this.dateTally = dateTally;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
