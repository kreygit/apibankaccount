package ovh.scarre.apiBankAccount.model;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonFilter;

@Entity
@JsonFilter("hidePassword")
public class User {

  @Id
  @SequenceGenerator(name = "SQ_USER", sequenceName = "SQ_USER", allocationSize = 1)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_USER")
  private Integer id;

  @Basic
  private String name;

  @Basic
  private String password;

  @ManyToMany(mappedBy = "userList")
  private List<Account> accountList;

  public User() {
    super();
  }

  public User(Integer pId, String pName, String pPassword) {
    super();
    this.id = pId;
    this.name = pName;
    this.password = pPassword;
  }

  public Integer getId() {
    return this.id;
  }

  public void setId(Integer pId) {
    this.id = pId;
  }

  public String getName() {
    return this.name;
  }

  public void setName(String pName) {
    this.name = pName;
  }

  public String getPassword() {
    return this.password;
  }

  public void setPassword(String pPassword) {
    this.password = pPassword;
  }

}
