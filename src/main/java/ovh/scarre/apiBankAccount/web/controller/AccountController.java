package ovh.scarre.apiBankAccount.web.controller;

import java.net.URI;
import java.util.List;

import javax.security.auth.login.AccountNotFoundException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ovh.scarre.apiBankAccount.dao.AccountDao;
import ovh.scarre.apiBankAccount.model.Account;

@Api(description = "API about Account")
@RestController
public class AccountController {

  @Autowired
  private AccountDao accountDao;

  @ApiOperation(value = "Get all accounts.")
  @CrossOrigin()
  @GetMapping(value = "/Account")
  public MappingJacksonValue getAllAccounts() throws AccountNotFoundException {
    List<Account> accounts = this.accountDao.findAll();
    if (accounts == null) {
      throw new AccountNotFoundException("No account found");
    } else {
      SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("password");
      FilterProvider filterList = new SimpleFilterProvider().addFilter("hidePassword", filter);
      MappingJacksonValue accountFilter = new MappingJacksonValue(accounts);
      accountFilter.setFilters(filterList);
      return accountFilter;
    }
  }

  @ApiOperation(value = "Get an account thanks to his ID except if not found.")
  @CrossOrigin()
  @GetMapping(value = "/Account/{pId}")
  public MappingJacksonValue getOneAccountById(@PathVariable int pId) throws AccountNotFoundException {
    Account account = this.accountDao.findById(pId);
    if (account == null) {
      throw new AccountNotFoundException("The account with id " + pId + " not found");
    } else {
      SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("password");
      FilterProvider filterList = new SimpleFilterProvider().addFilter("hidePassword", filter);
      MappingJacksonValue accountFilter = new MappingJacksonValue(account);
      accountFilter.setFilters(filterList);
      return accountFilter;
    }
  }

  @ApiOperation(value = "Get an account thanks to his number except if not found.")
  @CrossOrigin()
  @GetMapping(value = "/Account/Number/{pNumber}")
  public MappingJacksonValue getOneAccountByNumber(@PathVariable String pNumber) throws AccountNotFoundException {
    Account account = this.accountDao.findByNumber(pNumber);
    if (account == null) {
      throw new AccountNotFoundException("The account with number: " + pNumber + " not found");
    } else {
      SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("password");
      FilterProvider filterList = new SimpleFilterProvider().addFilter("hidePassword", filter);
      MappingJacksonValue accountFilter = new MappingJacksonValue(account);
      accountFilter.setFilters(filterList);
      return accountFilter;
    }
  }

  @ApiOperation(value = "Get an account thanks to his iban except if not found.")
  @CrossOrigin()
  @GetMapping(value = "/Account/Iban/{pIban}")
  public MappingJacksonValue getOneAccountByIban(@PathVariable String pIban) throws AccountNotFoundException {
    Account account = this.accountDao.findByIban(pIban);
    if (account == null) {
      throw new AccountNotFoundException("The account with iban: " + pIban + " not found");
    } else {
      SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("password");
      FilterProvider filterList = new SimpleFilterProvider().addFilter("hidePassword", filter);
      MappingJacksonValue accountFilter = new MappingJacksonValue(account);
      accountFilter.setFilters(filterList);
      return accountFilter;
    }
  }

  @ApiOperation(value = "Create an account")
  @CrossOrigin()
  @PostMapping(value = "/Account")
  public ResponseEntity<Void> createAccount(@Valid @RequestBody Account pAccount) {
    Account accountCreated = this.accountDao.save(pAccount);
    if (accountCreated == null) {
      return ResponseEntity.noContent().build();
    }
    URI location =
        ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(accountCreated.getId()).toUri();
    return ResponseEntity.created(location).build();
  }

  @ApiOperation(value = "Delete an account")
  @CrossOrigin()
  @DeleteMapping(value = "/Account/{pId}")
  public void deleteAccount(@PathVariable int pId) throws AccountNotFoundException {
    Account account = this.accountDao.findById(pId);
    if (account == null) {
      throw new AccountNotFoundException("The account with id " + pId + " not found");
    } else {
      this.accountDao.delete(account);
    }
  }

  @ApiOperation(value = "Update an account")
  @CrossOrigin()
  @PutMapping(value = "/Account")
  public void updateAccount(@RequestBody Account pAccount) throws AccountNotFoundException {
    Account account = this.accountDao.getOne(pAccount.getId());
    if (account == null) {
      throw new AccountNotFoundException("The account with id " + pAccount.getId() + " not found");
    } else {
      account = pAccount;
      this.accountDao.save(account);
    }

  }
}
