package ovh.scarre.apiBankAccount.web.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ovh.scarre.apiBankAccount.dao.OperationDao;
import ovh.scarre.apiBankAccount.model.Operation;
import ovh.scarre.apiBankAccount.web.exceptions.OperationNotFoundException;

@Api(description = "API about Operation")
@RestController
public class OperationController {

	@Autowired
	private OperationDao operationDao;

	@ApiOperation(value = "Get all operations.")
	@CrossOrigin()
	@GetMapping(value = "/Operation")
	public MappingJacksonValue getAllOperations() throws OperationNotFoundException {
		List<Operation> operations = this.operationDao.findAll();
		if (operations == null) {
			throw new OperationNotFoundException("No operations found");
		} else {
			SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("password");
			FilterProvider filterList = new SimpleFilterProvider().addFilter("hidePassword", filter);
			MappingJacksonValue operationFilter = new MappingJacksonValue(operations);
			operationFilter.setFilters(filterList);
			return operationFilter;
		}
	}

	@ApiOperation(value = "Get an operation thanks to his ID except if not found.")
	@CrossOrigin()
	@GetMapping(value = "/Operation/{pId}")
	public MappingJacksonValue getOneOperationById(@PathVariable int pId) throws OperationNotFoundException {
		Operation operation = this.operationDao.findById(pId);
		if (operation == null) {
			throw new OperationNotFoundException("The operation with id " + pId + " not found");
		} else {
			SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("password");
			FilterProvider filterList = new SimpleFilterProvider().addFilter("hidePassword", filter);
			MappingJacksonValue operationFilter = new MappingJacksonValue(operation);
			operationFilter.setFilters(filterList);
			return operationFilter;
		}
	}

	@ApiOperation(value = "Create an operation")
	@CrossOrigin()
	@PostMapping(value = "/Operation")
	public ResponseEntity<Void> createOperation(@Valid @RequestBody Operation pOperation) {
		Operation operationCreated = this.operationDao.save(pOperation);
		if (operationCreated == null) {
			return ResponseEntity.noContent().build();
		}
		URI location =
				ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(operationCreated.getId()).toUri();
		return ResponseEntity.created(location).build();
	}

	@ApiOperation(value = "Delete an operation")
	@CrossOrigin()
	@DeleteMapping(value = "/Operation/{pId}")
	public void deleteOperation(@PathVariable int pId) throws OperationNotFoundException {
		Operation operation = this.operationDao.findById(pId);
		if (operation == null) {
			throw new OperationNotFoundException("The operation with id " + pId + " not found");
		} else {
			this.operationDao.delete(operation);
		}
	}

	@ApiOperation(value = "Update an operation")
	@CrossOrigin()
	@PutMapping(value = "/Operation")
	public void updateOperation(@RequestBody Operation pOperation) throws OperationNotFoundException {
		Operation operation = this.operationDao.getOne(pOperation.getId());
		if (operation == null) {
			throw new OperationNotFoundException("The account with id " + pOperation.getId() + " not found");
		} else {
			operation = pOperation;
			this.operationDao.save(operation);
		}
	}
}
