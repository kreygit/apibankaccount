package ovh.scarre.apiBankAccount.web.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ovh.scarre.apiBankAccount.dao.PaymentDao;
import ovh.scarre.apiBankAccount.model.Payment;
import ovh.scarre.apiBankAccount.web.exceptions.PaymentNotFoundException;
import ovh.scarre.apiBankAccount.web.exceptions.UserNotFoundException;

@Api(description = "API about payment")
@RestController
public class PaymentController {

  @Autowired
  private PaymentDao paymentDao;

  @ApiOperation(value = "Get all paymentss.")
  @CrossOrigin()
  @GetMapping(value = "/Payment")
  public List<Payment> getAllPayments() {
    List<Payment> payments = this.paymentDao.findAll();
    if (payments == null) {
      throw new PaymentNotFoundException("No payments found");
    }
    return payments;
  }

  @ApiOperation(value = "Get a payment thanks to his ID except if not found.")
  @CrossOrigin()
  @GetMapping(value = "/Payment/{pId}")
  public Payment getOnePaymentById(@PathVariable int pId) {
    Payment payment = this.paymentDao.findById(pId);
    if (payment == null) {
      throw new PaymentNotFoundException("The payment with id " + pId + " not found");
    }
    return payment;
  }

  @ApiOperation(value = "Create a payment")
  @CrossOrigin()
  @PostMapping(value = "/Payment")
  public ResponseEntity<Void> createPayment(@Valid @RequestBody Payment pPayment) {
    Payment paymentCreated = this.paymentDao.save(pPayment);
    if (paymentCreated == null) {
      return ResponseEntity.noContent().build();
    }
    URI location =
        ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(paymentCreated.getId()).toUri();
    return ResponseEntity.created(location).build();
  }

  @ApiOperation(value = "Delete a payment")
  @CrossOrigin()
  @DeleteMapping(value = "/Payment/{pId}")
  public void deletePayment(@PathVariable int pId) {
    Payment payment = this.paymentDao.findById(pId);
    if (payment == null) {
      throw new UserNotFoundException("The payment with id " + pId + " not found");
    } else {
      this.paymentDao.delete(payment);
    }
  }

  @ApiOperation(value = "Update a payment")
  @CrossOrigin()
  @PutMapping(value = "/Payment")
  public void updatePayment(@RequestBody Payment pPayment) {
    Payment payment = this.paymentDao.getOne(pPayment.getId());
    if (payment == null) {
      throw new UserNotFoundException("The user with id " + pPayment.getId() + " not found");
    } else {
      payment = pPayment;
      this.paymentDao.save(payment);
    }
  }
}
