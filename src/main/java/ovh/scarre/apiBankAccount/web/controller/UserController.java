package ovh.scarre.apiBankAccount.web.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ovh.scarre.apiBankAccount.dao.UserDao;
import ovh.scarre.apiBankAccount.model.User;
import ovh.scarre.apiBankAccount.web.exceptions.UserNotFoundException;

@Api(description = "API about User")
@RestController
public class UserController {

  @Autowired
  private UserDao userDao;

  @ApiOperation(value = "Get an user thanks to his ID except if not found.")
  @CrossOrigin()
  @GetMapping(value = "/User/{id}")
  public MappingJacksonValue getOneUser(@PathVariable int pId) {
    User user = this.userDao.findById(pId);
    if (user == null) {
      throw new UserNotFoundException("The user with id " + pId + " not found");
    } else {
      SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("password");
      FilterProvider filterList = new SimpleFilterProvider().addFilter("hidePassword", filter);
      MappingJacksonValue userFilter = new MappingJacksonValue(user);
      userFilter.setFilters(filterList);
      return userFilter;
    }

  }

  @ApiOperation(value = "Get all users.")
  @CrossOrigin()
  @GetMapping(value = "/User")
  public MappingJacksonValue getAllUsers() {
    List<User> users = this.userDao.findAll();
    if (users == null) {
      throw new UserNotFoundException("No user found");
    } else {
      SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.serializeAllExcept("password");
      FilterProvider filterList = new SimpleFilterProvider().addFilter("hidePassword", filter);
      MappingJacksonValue usersFilter = new MappingJacksonValue(users);
      usersFilter.setFilters(filterList);
      return usersFilter;
    }
  }

  @ApiOperation(value = "Create an user")
  @CrossOrigin()
  @PostMapping(value = "/User")
  public ResponseEntity<Void> createUser(@Valid @RequestBody User user) {
    User userCreated = this.userDao.save(user);
    if (userCreated == null) {
      return ResponseEntity.noContent().build();
    }
    URI location =
        ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(userCreated.getId()).toUri();
    return ResponseEntity.created(location).build();
  }

  @ApiOperation(value = "Delete an user")
  @CrossOrigin()
  @DeleteMapping(value = "/User/{pId}")
  public void deleteUser(@PathVariable int pId) {
    User user = this.userDao.findById(pId);
    if (user == null) {
      throw new UserNotFoundException("The user with id " + pId + " not found");
    } else {
      this.userDao.delete(user);
    }
  }

  @ApiOperation(value = "Update an user")
  @CrossOrigin()
  @PutMapping(value = "/User")
  public void updateUser(@RequestBody User pUser) {
    User user = this.userDao.getOne(pUser.getId());
    if (user == null) {
      throw new UserNotFoundException("The user with id " + pUser.getId() + " not found");
    } else {
      user = pUser;
      this.userDao.save(user);
    }
  }
}
