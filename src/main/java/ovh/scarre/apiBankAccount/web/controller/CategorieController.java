package ovh.scarre.apiBankAccount.web.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import ovh.scarre.apiBankAccount.dao.CategorieDao;
import ovh.scarre.apiBankAccount.model.Categorie;
import ovh.scarre.apiBankAccount.web.exceptions.CategorieNotFoundException;
import ovh.scarre.apiBankAccount.web.exceptions.UserNotFoundException;

@Api(description = "API about Categorie")
@RestController
public class CategorieController {

  @Autowired
  private CategorieDao categorieDao;

  @ApiOperation(value = "Get all categories.")
  @CrossOrigin()
  @GetMapping(value = "/Categorie")
  public List<Categorie> getAllCategories() {
    List<Categorie> categories = this.categorieDao.findAll();
    if (categories == null) {
      throw new CategorieNotFoundException("No categories found");
    }
    return categories;
  }

  @ApiOperation(value = "Get a categorie thanks to his ID except if not found.")
  @CrossOrigin()
  @GetMapping(value = "/Categorie/{pId}")
  public Categorie getOneCategorieById(@PathVariable int pId) {
    Categorie categorie = this.categorieDao.findById(pId);
    if (categorie == null) {
      throw new CategorieNotFoundException("The categorie with id " + pId + " not found");
    }
    return categorie;
  }

  @ApiOperation(value = "Create a categorie")
  @CrossOrigin()
  @PostMapping(value = "/Categorie")
  public ResponseEntity<Void> createCategorie(@Valid @RequestBody Categorie pCategorie) {
    Categorie categorieCreated = this.categorieDao.save(pCategorie);
    if (categorieCreated == null) {
      return ResponseEntity.noContent().build();
    }
    URI location =
        ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(categorieCreated.getId()).toUri();
    return ResponseEntity.created(location).build();
  }

  @ApiOperation(value = "Delete a categorie")
  @CrossOrigin()
  @DeleteMapping(value = "/Categorie/{pId}")
  public void deleteCategorie(@PathVariable int pId) {
    Categorie categorie = this.categorieDao.findById(pId);
    if (categorie == null) {
      throw new UserNotFoundException("The categorie with id " + pId + " not found");
    } else {
      this.categorieDao.delete(categorie);
    }
  }

  @ApiOperation(value = "Update a categorie")
  @CrossOrigin()
  @PutMapping(value = "/Categorie")
  public void updateCategorie(@RequestBody Categorie pCategorie) {
    Categorie categorie = this.categorieDao.getOne(pCategorie.getId());
    if (categorie == null) {
      throw new UserNotFoundException("The categorie with id " + pCategorie.getId() + " not found");
    } else {
      categorie = pCategorie;
      this.categorieDao.save(categorie);
    }
  }
}
