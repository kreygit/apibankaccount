package ovh.scarre.apiBankAccount.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PaymentNotFoundException extends RuntimeException {

  /** le serialVersionUID */
  private static final long serialVersionUID = -6630790402701192144L;

  public PaymentNotFoundException(String pMessage) {
    super(pMessage);
  }

}
