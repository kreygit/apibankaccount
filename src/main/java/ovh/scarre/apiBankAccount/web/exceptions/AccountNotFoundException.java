package ovh.scarre.apiBankAccount.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AccountNotFoundException extends RuntimeException {

  /** le serialVersionUID */
  private static final long serialVersionUID = -890619906352713471L;

  public AccountNotFoundException(String pMessage) {
    super(pMessage);
  }
}
