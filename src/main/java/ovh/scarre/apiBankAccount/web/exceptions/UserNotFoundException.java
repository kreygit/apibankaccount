package ovh.scarre.apiBankAccount.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends RuntimeException {

  /** le serialVersionUID */
  private static final long serialVersionUID = 2720591698415646695L;

  public UserNotFoundException(String pMessage) {
    super(pMessage);
  }
}
