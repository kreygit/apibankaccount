package ovh.scarre.apiBankAccount.web.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CategorieNotFoundException extends RuntimeException {

  /** le serialVersionUID */
  private static final long serialVersionUID = 997611963502319627L;

  public CategorieNotFoundException(String pMessage) {
    super(pMessage);
  }

}
